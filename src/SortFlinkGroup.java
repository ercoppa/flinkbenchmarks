import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.util.Collector;

import java.util.LinkedList;

public class SortFlinkGroup {

	public static void main(String[] args) throws Exception {

		if (args.length < 3) {
			System.out.println("Wrong arguments: " + args[0] + " <input> <output>");
			System.exit(1);
		}

		String inputPath = args[1];
		String outputPath = args[2];

		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

		DataSet<String> lines = getTextDataSet(env, inputPath); // input to dataset of lines
		DataSet<String> result = lines.map(new Tupler())
				.groupBy(0).sortGroup(1, Order.ASCENDING).reduceGroup(new Emitter());

		result.writeAsText(outputPath, FileSystem.WriteMode.OVERWRITE);
		env.execute("Sort");
	}

	private static DataSet<String> getTextDataSet(ExecutionEnvironment env, String file) {
		return env.readTextFile(file);
	}

	public static class Tupler implements MapFunction<String, Tuple3<Boolean, Float, String>> {

		public static Float getIncome(String line) {

			// this is just to avoid line.split(";")
			// which will create many string tokens

			// income is the 25th fields in the line
			int count = 0;
			int start = 0;
			int end = 0;
			for (int i = 0; i < line.length(); i++) {
				if (line.charAt(i) == ',') {
					count += 1;
					if (count == 24) {
						start = i + 1;
					} else if (count == 25) {
						end = i;
						break;
					}
				}
			}

			return Float.valueOf(line.substring(start, end));
		}

		@Override
		public Tuple3<Boolean, Float, String> map(String s) throws Exception {
			return new Tuple3<Boolean, Float, String>(true, getIncome(s), s);
		}
	}

	public static final class Emitter
			implements GroupReduceFunction<Tuple3<Boolean, Float, String>, String> {

		@Override
		public void reduce(
				Iterable<Tuple3<Boolean, Float, String>> iterable,
				Collector<String> collector) throws Exception {

			for (Tuple3<Boolean, Float, String> i : iterable)
				collector.collect(i.f2);

		}
	}
}