import edu.purdue.cs.aggr.NOAHReduceFunction;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.functions.MapPartitionFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.util.Collector;

import java.util.Comparator;
import java.util.PriorityQueue;

public class YahooMusicTopK {

	public final static int K = 6249 * 10; // 10% of N

	public static void main(String[] args) throws Exception {

		if (args.length < 4) {
			System.out.println("Wrong arguments: " + args[0] + " <input> <output> <useNoah>");
			System.exit(1);
		}

		String inputPath = args[1];
		String outputPath = args[2];
		String useNoah = args[3];

		ReduceFunction<PriorityQueue<Tuple2<Float, Integer>>> merger = null;
		if (useNoah.equals("yes"))
			merger = new MergerNOAH();
		else
			merger = new MergerFlink();

		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

		DataSet<String> lines = getTextDataSet(env, inputPath); // input to dataset of lines
		DataSet<String> result = lines
				.mapPartition(new Emitter())
				.groupBy(0)
				.reduceGroup(new Merger())
				.mapPartition(new Sorter()) // get a partition and return a queue with top K elements
				.reduce(merger) // get different queues, extract top k elements
				.flatMap(new Splitter());

		// N.B. we need flatMap() because writeAsText() calls toString() on each
		// element of the dataset: if the dataset contains a LinkedList
		// then LinkedList.toString() is called and this could be very expensive
		// when the list is very large

		result.writeAsText(outputPath, FileSystem.WriteMode.OVERWRITE);
		env.execute("YahooMusicTopK");
	}

	private static DataSet<String> getTextDataSet(ExecutionEnvironment env, String file) {
		return env.readTextFile(file);
	}

	public static final class Emitter implements
			MapPartitionFunction<String, Tuple2<Integer, Integer>> {

		public static Tuple2<Integer, Integer> getTuple(String line) {

			// this is just to avoid line.split(" ")
			// which will create many string tokens

			int count = 0;
			int start = -1;
			boolean prevIsSpace = false;
			int songId = -1;
			int score = -1;
			for (int i = 0; i < line.length(); i++) {

				if (line.charAt(i) == '\t') {

					if (!prevIsSpace) {

						if (count == 0) {
							songId = Integer.parseInt(line.substring(0, i));
						}

						if (count == 1) {
							score = Integer.parseInt(line.substring(start, i));
							break;
						}

					}

					prevIsSpace = true;

				} else {

					if (prevIsSpace) {
						count += 1;
						start = i;
					}

					prevIsSpace = false;
				}
			}

			if (count == 0)
				return null;

			return new Tuple2<Integer, Integer>(songId, score);
		}

		@Override
		public void mapPartition(Iterable<String> iterable,
				Collector<Tuple2<Integer, Integer>> collector) throws Exception {

			Tuple2<Integer, Integer> t;
			for (String s : iterable) {

				t = getTuple(s);
				if (t != null)
					collector.collect(t);

			}

		}
	}

	public static final class Merger
			implements GroupReduceFunction<Tuple2<Integer, Integer>, Tuple2<Float, Integer>> {

		@Override
		public void reduce(
				Iterable<Tuple2<Integer, Integer>> iterable,
				Collector<Tuple2<Float, Integer>> collector) throws Exception {

			float sum = 0f;
			int count = 0;
			int id = 0;
			for (Tuple2<Integer, Integer> elem : iterable) {

				sum += elem.f1;
				count += 1;
				id = elem.f0;

			}

			//System.out.println("SongId " + id + " avgScore " + (sum / count));
			collector.collect(new Tuple2<Float, Integer>(sum / count, id));
		}
	}

	public static final class Sorter implements
			MapPartitionFunction<Tuple2<Float, Integer>, PriorityQueue<Tuple2<Float, Integer>>> {

		@Override
		public void mapPartition(Iterable<Tuple2<Float, Integer>> items,
				Collector<PriorityQueue<Tuple2<Float, Integer>>> out) {

			PriorityQueue<Tuple2<Float, Integer>> result =
					new PriorityQueue<Tuple2<Float, Integer>>(K, new RankComparator());

			for (Tuple2<Float, Integer> t : items) {

				//System.out.println("SongId " + t.f1 + " avgScore " + t.f0);

				if (result.size() < K)
					result.offer(new Tuple2<Float, Integer>(t.f0, t.f1));
				else if (result.peek().f0 < t.f0) {
					result.poll();
					result.offer(new Tuple2<Float, Integer>(t.f0, t.f1));
				}
			}

			out.collect(result);
		}
	}

	public static class RankComparator implements
			Comparator<Tuple2<Float, Integer>> {

		@Override
		public int compare(Tuple2<Float, Integer> a, Tuple2<Float, Integer> b) {
			return a.f0.compareTo(b.f0);
		}
	}

	public static final class MergerNOAH implements
			NOAHReduceFunction<PriorityQueue<Tuple2<Float, Integer>>> {

		@Override
		public float getInputOutputRatio(int numberOfPartitions) {
			return 1;
		}

		@Override
		public PriorityQueue<Tuple2<Float, Integer>> reduce(PriorityQueue<Tuple2<Float, Integer>> a,
				PriorityQueue<Tuple2<Float, Integer>> b) throws Exception {

			// this is the approach used by Spark in top()

			for (Tuple2<Float, Integer> elem : b) {

				if (a.size() < K)
					a.offer(elem);
				else if (elem.f0 > a.peek().f0) {
					a.poll();
					a.offer(elem);
				}
			}

			//System.out.println(a);

			return a;
		}
	}

	public static final class MergerFlink implements
			ReduceFunction<PriorityQueue<Tuple2<Float, Integer>>> {

		@Override
		public PriorityQueue<Tuple2<Float, Integer>> reduce(PriorityQueue<Tuple2<Float, Integer>> a,
				PriorityQueue<Tuple2<Float, Integer>> b) throws Exception {

			// this is the approach used by Spark in top()

			for (Tuple2<Float, Integer> elem : b) {

				if (a.size() < K)
					a.offer(elem);
				else if (elem.f0 > a.peek().f0) {
					a.poll();
					a.offer(elem);
				}
			}

			return a;
		}
	}

	public static final class Splitter implements
			FlatMapFunction<PriorityQueue<Tuple2<Float, Integer>>, String> {

		@Override
		public void flatMap(PriorityQueue<Tuple2<Float, Integer>> queue,
				Collector<String> collector) throws Exception {

			while (queue.size() > 0) {
				Tuple2<Float, Integer> t = queue.poll();
				collector.collect(t.f0 + "\t" + t.f1);
			}

		}
	}

}
