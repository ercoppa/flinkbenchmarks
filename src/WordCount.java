import edu.purdue.cs.aggr.NOAHReduceFunction;
import org.apache.flink.api.common.functions.MapPartitionFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.typeinfo.BasicTypeInfo;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.HashMap;

public class WordCount {

	public static void main(String[] args) throws Exception {

		// set up the execution environment
		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

		String inputPath = null;
		String outputPath = null;

		// get input data
		DataSet<String> text = getTextDataSet(env, inputPath);

		DataSet<HashMap<String, Integer>> counts =
				// split up the lines in pairs (2-tuples) containing: (word,1)
				text.mapPartition(new Tokenizer())
						// group by the tuple field "0" and sum up tuple field "1"
						.reduce(new Reducer());

		if(outputPath != null) {
			counts.writeAsCsv(outputPath, "\n", " ");
		} else {
			counts.print();
		}

		// execute program
		env.execute("WordCount");
	}

	public static final class Tokenizer implements
			MapPartitionFunction<String, HashMap<String, Integer>> {

		@Override
		public void mapPartition(Iterable<String> values, Collector<HashMap<String, Integer>> out) {

			HashMap<String, Integer> result = new HashMap<String, Integer>();

			for (String value : values) {

				// normalize and split the line
				String[] tokens = value.toLowerCase().split("\\W+");

				// emit the pairs
				for (String token : tokens) {
					if (token.length() > 0) {
						Integer count = result.get(token);
						if (count == null)
							result.put(token, 1);
						else
							result.put(token, 1 + count);
					}
				}
			}

			out.collect(result);
		}
	}

	public static final class Reducer implements
			NOAHReduceFunction<HashMap<String, Integer>> {

		@Override
		public HashMap<String, Integer> reduce(
				HashMap<String, Integer> a,
				HashMap<String, Integer> b) throws Exception {

			for (String word : b.keySet()) {

				Integer count = a.get(word);
				if (count != null)
					a.put(word, count + b.get(word));
				else
					a.put(word, b.get(word));

			}

			return a;
		}

		@Override
		public float getInputOutputRatio(int numPartitions) {
			return 1.0f;
		}
	}

	private static DataSet<String> getTextDataSet(ExecutionEnvironment env, String file) {
		if(file != null) {
			// read the text file from given input path
			return env.readTextFile(file);
		} else {
			// get default test text data

			ArrayList<String> words = new ArrayList<String>();
			for (String word : WORDS)
				words.add(word);

			return env.fromParallelCollection(new wordDataCountGenerator(words, 0, words.size()), BasicTypeInfo.STRING_TYPE_INFO);
			//return getDefaultTextLineDataSet(env);
		}
	}

	public static final String[] WORDS = new String[] {
			"To be, or not to be,--that is the question:--",
			"Whether 'tis nobler in the mind to suffer",
			"The slings and arrows of outrageous fortune",
			"Or to take arms against a sea of troubles,",
			"And by opposing end them?--To die,--to sleep,--",
			"No more; and by a sleep to say we end",
			"The heartache, and the thousand natural shocks",
			"That flesh is heir to,--'tis a consummation",
			"Devoutly to be wish'd. To die,--to sleep;--",
			"To sleep! perchance to dream:--ay, there's the rub;",
			"For in that sleep of death what dreams may come,",
			"When we have shuffled off this mortal coil,",
			"Must give us pause: there's the respect",
			"That makes calamity of so long life;",
			"For who would bear the whips and scorns of time,",
			"The oppressor's wrong, the proud man's contumely,",
			"The pangs of despis'd love, the law's delay,",
			"The insolence of office, and the spurns",
			"That patient merit of the unworthy takes,",
			"When he himself might his quietus make",
			"With a bare bodkin? who would these fardels bear,",
			"To grunt and sweat under a weary life,",
			"But that the dread of something after death,--",
			"The undiscover'd country, from whose bourn",
			"No traveller returns,--puzzles the will,",
			"And makes us rather bear those ills we have",
			"Than fly to others that we know not of?",
			"Thus conscience does make cowards of us all;",
			"And thus the native hue of resolution",
			"Is sicklied o'er with the pale cast of thought;",
			"And enterprises of great pith and moment,",
			"With this regard, their currents turn awry,",
			"And lose the name of action.--Soft you now!",
			"The fair Ophelia!--Nymph, in thy orisons",
			"Be all my sins remember'd."
	};

	public static DataSet<String> getDefaultTextLineDataSet(ExecutionEnvironment env) {
		return env.fromElements(WORDS);
	}
}
