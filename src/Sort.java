import edu.purdue.cs.aggr.NOAHReduceFunction;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapPartitionFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.util.Collector;

import java.util.*;

public class Sort {

	public static void main(String[] args) throws Exception {

		if (args.length < 4) {
			System.out.println("Wrong arguments: " + args[0] + " <input> <output> <useNoah>");
			System.exit(1);
		}

		String inputPath = args[1];
		String outputPath = args[2];
		String useNoah = args[3];

		ReduceFunction<LinkedList<Tuple2<Float, String>>> merger = null;
		if (useNoah.equals("yes"))
			merger = new MergerNOAH();
		else
			merger = new MergerFlink();

		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

		DataSet<String> lines = getTextDataSet(env, inputPath); // input to dataset of lines
		DataSet<String> result = lines
				.mapPartition(new Sorter()) // get a partition and return a sorted list
				.reduce(merger) // merge lists
				.flatMap(new Splitter()); // list to dataset of lines

		// N.B. we need flatMap() because writeAsText() calls toString() on each
		// element of the dataset: if the dataset contains a LinkedList
		// then LinkedList.toString() is called and this could be very expensive
		// when the list is very large

		result.writeAsText(outputPath, FileSystem.WriteMode.OVERWRITE);
		env.execute("Sort");
	}

	private static DataSet<String> getTextDataSet(ExecutionEnvironment env, String file) {
		return env.readTextFile(file);
	}

	public static final class Sorter implements
			MapPartitionFunction<String, LinkedList<Tuple2<Float, String>>> {

		public static Float getIncome(String line) {

			// this is just to avoid line.split(";")
			// which will create many string tokens

			// income is the 25th fields in the line
			int count = 0;
			int start = 0;
			int end = 0;
			for (int i = 0; i < line.length(); i++) {
				if (line.charAt(i) == ',') {
					count += 1;
					if (count == 24) {
						start = i + 1;
					} else if (count == 25) {
						end = i;
						break;
					}
				}
			}

			return Float.valueOf(line.substring(start, end));
		}

		@Override
		public void mapPartition(Iterable<String> lines, Collector<LinkedList<Tuple2<Float, String>>> out) {

			LinkedList<Tuple2<Float, String>> result = new LinkedList<Tuple2<Float, String>>();
			for (String line : lines) {
				Tuple2<Float, String> t = new Tuple2<Float, String>(getIncome(line), line);
				result.add(t);
			}

			Collections.sort(result, new IncomeComparator());
			out.collect(result);
		}
	}

	public static class IncomeComparator implements Comparator<Tuple2<Float, String>> {

		@Override
		public int compare(Tuple2<Float, String> a, Tuple2<Float, String> b) {
			return a.f0.compareTo(b.f0);
		}
	}


	public static final class MergerNOAH implements
			NOAHReduceFunction<LinkedList<Tuple2<Float, String>>> {

		@Override
		public float getInputOutputRatio(int numberOfPartitions) {
			return numberOfPartitions;
		}

		@Override
		public LinkedList<Tuple2<Float, String>> reduce(LinkedList<Tuple2<Float, String>> a,
				LinkedList<Tuple2<Float, String>> b) throws Exception {

			LinkedList<Tuple2<Float, String>> result = new LinkedList<Tuple2<Float, String>>();
			while (a.size() > 0 && b.size() > 0) {

				if (a.getFirst().f0.compareTo(b.getFirst().f0) <= 0)
					result.add(a.poll());
				else
					result.add(b.poll());

			}

			while (a.size() > 0)
				result.add(a.poll());

			while (b.size() > 0)
				result.add(b.poll());

			return result;
		}
	}

	public static final class MergerFlink implements
			ReduceFunction<LinkedList<Tuple2<Float, String>>> {

		@Override
		public LinkedList<Tuple2<Float, String>> reduce(LinkedList<Tuple2<Float, String>> a,
				LinkedList<Tuple2<Float, String>> b) throws Exception {

			LinkedList<Tuple2<Float, String>> result = new LinkedList<Tuple2<Float, String>>();
			while (a.size() > 0 && b.size() > 0) {

				if (a.getFirst().f0.compareTo(b.getFirst().f0) <= 0)
					result.add(a.poll());
				else
					result.add(b.poll());

			}

			while (a.size() > 0)
				result.add(a.poll());

			while (b.size() > 0)
				result.add(b.poll());

			return result;
		}
	}

	public static final class Splitter implements
			FlatMapFunction<LinkedList<Tuple2<Float, String>>, String> {

		@Override
		public void flatMap(LinkedList<Tuple2<Float, String>> list,
				Collector<String> collector) throws Exception {

			for (Tuple2<Float, String> t : list) {
				collector.collect(t.f1);
			}

		}
	}

}
