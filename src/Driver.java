public class Driver {

	public static void main(String[] args) throws Exception {

		System.out.println(args[0]);

		if (args[0].equals("WordCount"))
			WordCount.main(args);
		else if (args[0].equals("RandomSummer"))
			RandomSummer.main(args);
		else if (args[0].equals("Sort"))
			Sort.main(args);
		else if (args[0].equals("SortFlinkGroup")) {
			SortFlinkGroup.main(args);
		} else if (args[0].equals("SortFlinkGroupCombine")) {
			SortFlinkGroupCombine.main(args);
		} else if (args[0].equals("ImdbTopK")) {
			ImdbTopK.main(args);
		} else if (args[0].equals("ImdbTopKFlink")) {
			ImdbTopKFlink.main(args);
		} else if (args[0].equals("YahooMusicTopK")) {
			YahooMusicTopK.main(args);
		} else if (args[0].equals("YahooMusicTopKFlink")) {
			YahooMusicTopKFlink.main(args);
		} else {
			System.err.println("Invalid benchmark");
			System.exit(1);
		}

	}

}
