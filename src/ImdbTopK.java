import edu.purdue.cs.aggr.NOAHReduceFunction;
import org.apache.flink.api.common.functions.*;
import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.util.Collector;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;

public class ImdbTopK {

	public final static int K = 6262 * 10; // 10% of N
	public final static float AVG_RANK = 6.90f;
	public final static int MIN_NUM_OF_VOTES = 25000;

	public static void main(String[] args) throws Exception {

		if (args.length < 4) {
			System.out.println("Wrong arguments: " + args[0] + " <input> <output> <useNoah>");
			System.exit(1);
		}

		String inputPath = args[1];
		String outputPath = args[2];
		String useNoah = args[3];

		ReduceFunction<PriorityQueue<Tuple2<Float, String>>> merger = null;
		if (useNoah.equals("yes"))
			merger = new MergerNOAH();
		else
			merger = new MergerFlink();

		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

		DataSet<String> lines = getTextDataSet(env, inputPath); // input to dataset of lines
		DataSet<String> result = lines
				.mapPartition(new Sorter()) // get a partition and return a queue with top K elements
				.reduce(merger) // get different queues, extract top k elements
				.flatMap(new Splitter()); // list to dataset of lines

		// N.B. we need flatMap() because writeAsText() calls toString() on each
		// element of the dataset: if the dataset contains a LinkedList
		// then LinkedList.toString() is called and this could be very expensive
		// when the list is very large

		result.writeAsText(outputPath, FileSystem.WriteMode.OVERWRITE);
		env.execute("ImdbTopK");
	}

	private static DataSet<String> getTextDataSet(ExecutionEnvironment env, String file) {
		return env.readTextFile(file);
	}

	public static final class Sorter implements
			MapPartitionFunction<String, PriorityQueue<Tuple2<Float, String>>> {

		public static Float getWeighedRank(String line) {

			// this is just to avoid line.split(" ")
			// which will create many string tokens

			int count = 0;
			int start = -1;
			boolean prevIsSpace = true;
			float numOfVotes = 0;
			float rank = 0f;
			for (int i = 0; i < line.length(); i++) {

				if (line.charAt(i) == ' ') {

					if (start > 0) {
						if (count == 2) {
							numOfVotes = Float.parseFloat(line.substring(start, i));
							start = -1;
						} else {
							rank = Float.valueOf(line.substring(start, i));
							break;
						}
					}
					prevIsSpace = true;

				} else {

					if (prevIsSpace) {
						count += 1;
						if (count >= 2)
							start = i;
					}

					prevIsSpace = false;
				}
			}

			/*
				weighted rank = (v/(v+k))*X + (k/(v+k))*C

				where:

				X = average for the movie (mean)
				v = number of votes for the movie
				k = minimum votes required to be listed in the top 250 (currently 25000)
				C = the mean vote across the whole report (currently 6.90)
			*/
			rank = (numOfVotes / (numOfVotes + MIN_NUM_OF_VOTES)) * rank
					+ (MIN_NUM_OF_VOTES / (numOfVotes + MIN_NUM_OF_VOTES)) * AVG_RANK;

			return rank;
		}

		public static String buildPayload(String line, int times) {
			if (times == 1)
				return line;

			String s = line;
			for (int k = 0; k < times - 1; k++)
				s += line;

			return s;
		}

		@Override
		public void mapPartition(Iterable<String> lines, Collector<PriorityQueue<Tuple2<Float, String>>> out) {

			PriorityQueue<Tuple2<Float, String>> result =
					new PriorityQueue<Tuple2<Float, String>>(K, new RankComparator());

			for (String line : lines) {

				Tuple2<Float, String> t = new Tuple2<Float, String>(getWeighedRank(line), buildPayload(line, 1));
				if (result.size() < K)
					result.offer(t);
				else if (result.peek().f0 < t.f0) {
					result.poll();
					result.offer(t);
				}
			}

			out.collect(result);
		}
	}

	public static class RankComparator implements Comparator<Tuple2<Float, String>> {

		@Override
		public int compare(Tuple2<Float, String> a, Tuple2<Float, String> b) {
			return a.f0.compareTo(b.f0);
		}
	}

	public static final class MergerNOAH implements
			NOAHReduceFunction<PriorityQueue<Tuple2<Float, String>>> {

		@Override
		public float getInputOutputRatio(int numberOfPartitions) {
			return 1;
		}

		@Override
		public PriorityQueue<Tuple2<Float, String>> reduce(PriorityQueue<Tuple2<Float, String>> a,
				PriorityQueue<Tuple2<Float, String>> b) throws Exception {

			// this is the approach used by Spark in top()

			for (Tuple2<Float, String> elem : b) {

				if (a.size() < K)
					a.offer(elem);
				else if (elem.f0 > a.peek().f0) {
					a.poll();
					a.offer(elem);
				}
			}

			return a;
		}
	}

	public static final class MergerFlink implements
			ReduceFunction<PriorityQueue<Tuple2<Float, String>>> {

		@Override
		public PriorityQueue<Tuple2<Float, String>> reduce(PriorityQueue<Tuple2<Float, String>> a,
				PriorityQueue<Tuple2<Float, String>> b) throws Exception {

			// this is the approach used by Spark in top()

			for (Tuple2<Float, String> elem : b) {

				if (a.size() < K)
					a.offer(elem);
				else if (elem.f0 > a.peek().f0) {
					a.poll();
					a.offer(elem);
				}
			}

			return a;
		}
	}

	public static final class Splitter implements
			FlatMapFunction<PriorityQueue<Tuple2<Float, String>>, String> {

		@Override
		public void flatMap(PriorityQueue<Tuple2<Float, String>> queue,
				Collector<String> collector) throws Exception {

			while (queue.size() > 0) {
				collector.collect(queue.poll().f1);
			}

		}
	}

}
