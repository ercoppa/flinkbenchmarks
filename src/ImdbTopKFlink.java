import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.MapPartitionFunction;
import org.apache.flink.api.common.functions.RichGroupReduceFunction;
import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.util.Collector;

public class ImdbTopKFlink {

	public static void main(String[] args) throws Exception {

		if (args.length < 3) {
			System.out.println("Wrong arguments: " + args[0] + " <input> <output>");
			System.exit(1);
		}

		String inputPath = args[1];
		String outputPath = args[2];

		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

		DataSet<String> lines = env.readTextFile(inputPath); // input to dataset of lines
		DataSet<String> result = lines.mapPartition(new Tupler())
				.groupBy(0).sortGroup(1, Order.DESCENDING)
				.reduceGroup(new Emitter(ImdbTopK.K)).setParallelism(1);

		result.writeAsText(outputPath, FileSystem.WriteMode.OVERWRITE);
		env.execute("Sort");
	}

	public static class Tupler implements
			MapPartitionFunction<String, Tuple3<Boolean, Float, String>> {

		@Override
		public void mapPartition(Iterable<String> iterable,
				Collector<Tuple3<Boolean, Float, String>> collector) throws Exception {

			for (String s : iterable)
				collector.collect(new Tuple3<Boolean, Float, String>(true, ImdbTopK.Sorter.getWeighedRank(s), ImdbTopK.Sorter.buildPayload(s, 1)));

		}
	}

	@RichGroupReduceFunction.Combinable
	public static final class Emitter
			extends RichGroupReduceFunction<Tuple3<Boolean, Float, String>, String> {

		private final int count;

		public Emitter(int k) {
			this.count = k;
		}

		public void combine(Iterable<Tuple3<Boolean, Float, String>> iterable,
				Collector<Tuple3<Boolean, Float, String>> out) throws Exception {

			int numEmmitted = 0;
			for (Tuple3<Boolean, Float, String> i : iterable) {
				out.collect(i);
				numEmmitted += 1;
				if (numEmmitted == count)
					break;
			}
		}

		@Override
		public void reduce(
				Iterable<Tuple3<Boolean, Float, String>> iterable,
				Collector<String> collector) throws Exception {

			int numEmmitted = 0;
			for (Tuple3<Boolean, Float, String> i : iterable) {
				collector.collect(i.f2);
				numEmmitted += 1;
				if (numEmmitted == count)
					break;
			}
		}
	}
}
