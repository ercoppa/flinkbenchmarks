import org.apache.flink.util.SplittableIterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class wordDataCountGenerator implements SplittableIterator<String> {

	ArrayList<String> lines = new ArrayList<String>();
	int index = 0;

	public wordDataCountGenerator(ArrayList<String> lines, int from, int to) {

		for (int i = from; i < to; i++)
			this.lines.add(lines.get(i));

	}

	@Override
	public Iterator<String>[] split(int numSplits) {

		if (numSplits == 1) {
			return new wordDataCountGenerator[] {
					new wordDataCountGenerator(lines, index, lines.size())
			};

		}

		if (numSplits == 2) {

			int numElements = lines.size() - index;
			return new wordDataCountGenerator[] {
					new wordDataCountGenerator(lines, index, numElements / 2),
					new wordDataCountGenerator(lines, index + (numElements / 2), lines.size()),
			};

		}

		throw new RuntimeException("Not Supported");
	}

	@Override
	public Iterator<String> getSplit(int num, int numPartitions) {
		if (numPartitions < 1 || num < 0 || num >= numPartitions) {
			throw new IllegalArgumentException();
		}

		return split(numPartitions)[num];
	}

	@Override
	public int getMaximumNumberOfSplits() {
		return lines.size() - index;
	}

	@Override
	public boolean hasNext() {
		return index < lines.size();
	}

	@Override
	public String next() {
		if (index <= lines.size()) {
			return lines.get(index++);
		} else {
			throw new NoSuchElementException();
		}
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
