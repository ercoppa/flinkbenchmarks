import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.functions.MapPartitionFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichGroupReduceFunction;
import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.util.Collector;

import java.util.PriorityQueue;

/**
 * Created by ercoppa on 06/06/15.
 */
public class YahooMusicTopKFlink {

	public static void main(String[] args) throws Exception {

		if (args.length < 3) {
			System.out.println("Wrong arguments: " + args[0] + " <input> <output>");
			System.exit(1);
		}

		String inputPath = args[1];
		String outputPath = args[2];

		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

		DataSet<String> lines = getTextDataSet(env, inputPath); // input to dataset of lines
		DataSet<String> result = lines
				.mapPartition(new Emitter())
				.groupBy(0)
				.reduceGroup(new Merger())
				.groupBy(0)
				.sortGroup(1, Order.DESCENDING)
				.reduceGroup(new First(YahooMusicTopK.K)).setParallelism(1);

		result.writeAsText(outputPath, FileSystem.WriteMode.OVERWRITE);
		env.execute("YahooMuisicTopKFlink");
	}

	private static DataSet<String> getTextDataSet(ExecutionEnvironment env, String file) {
		return env.readTextFile(file);
	}

	public static final class Emitter implements
			MapPartitionFunction<String, Tuple2<Integer, Integer>> {

		public static Tuple2<Integer, Integer> getTuple(String line) {

			// this is just to avoid line.split(" ")
			// which will create many string tokens

			int count = 0;
			int start = -1;
			boolean prevIsSpace = false;
			int songId = -1;
			int score = -1;
			for (int i = 0; i < line.length(); i++) {

				if (line.charAt(i) == '\t') {

					if (!prevIsSpace) {

						if (count == 0) {
							songId = Integer.parseInt(line.substring(0, i));
						}

						if (count == 1) {
							score = Integer.parseInt(line.substring(start, i));
							break;
						}

					}

					prevIsSpace = true;

				} else {

					if (prevIsSpace) {
						count += 1;
						start = i;
					}

					prevIsSpace = false;
				}
			}

			if (count == 0)
				return null;

			return new Tuple2<Integer, Integer>(songId, score);
		}

		@Override
		public void mapPartition(Iterable<String> iterable,
				Collector<Tuple2<Integer, Integer>> collector) throws Exception {

			Tuple2<Integer, Integer> t;
			for (String s : iterable) {

				t = getTuple(s);
				if (t != null)
					collector.collect(t);

			}

		}
	}

	public static final class Merger
			implements
			GroupReduceFunction<Tuple2<Integer, Integer>, Tuple3<Boolean, Float, Integer>> {

		@Override
		public void reduce(
				Iterable<Tuple2<Integer, Integer>> iterable,
				Collector<Tuple3<Boolean, Float, Integer>> collector) throws Exception {

			float sum = 0f;
			int count = 0;
			int id = 0;
			for (Tuple2<Integer, Integer> elem : iterable) {

				sum += elem.f1;
				count += 1;
				id = elem.f0;

			}

			//System.out.println("SongId " + id + " avgScore " + (sum / count));
			collector.collect(new Tuple3<Boolean, Float, Integer>(true, sum / count, id));
		}
	}

	@RichGroupReduceFunction.Combinable
	public static final class First
			extends RichGroupReduceFunction<Tuple3<Boolean, Float, Integer>, String> {

		private final int count;

		public First(int k) {
			this.count = k;
		}

		public void combine(Iterable<Tuple3<Boolean, Float, Integer>> iterable,
				Collector<Tuple3<Boolean, Float, Integer>> out) throws Exception {

			int numEmmitted = 0;
			for (Tuple3<Boolean, Float, Integer> i : iterable) {
				out.collect(i);
				numEmmitted += 1;
				if (numEmmitted == count)
					break;
			}
		}

		@Override
		public void reduce(
				Iterable<Tuple3<Boolean, Float, Integer>> iterable,
				Collector<String> collector) throws Exception {

			int numEmmitted = 0;
			for (Tuple3<Boolean, Float, Integer> i : iterable) {
				collector.collect(i.f1 + "\t" + i.f2);
				numEmmitted += 1;
				if (numEmmitted == count)
					break;
			}
		}
	}
}
